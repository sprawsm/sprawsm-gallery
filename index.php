<!DOCTYPE html>
<html lang="en">

  <head>
      <title>Superawesome Gallery</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

      <style type="text/css">
        
        * {
          margin:  0;
          padding:  0;
          box-sizing: border-box;
        }

        body,
        html {
          position: relative;
          width: 100%;
          height: 100%;
          font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Fira Sans", "Droid Sans", "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
          font-size: 15px;
          line-height: 1.5;
        }

        .page-header {
          padding:  40px 20px;
          z-index: 9999;
        }

        h1 {
          font-size: 30px;
          font-weight: normal;
          line-height: 1;
          margin-bottom: .5em;
        }

        @media screen and (min-width: 800px) {
          .page-header { width: 50%;  }
        }

        .gallery {
          column-count: 1;
          column-gap: 0;
          padding:  10px;
        }

        @media screen and (min-width: 400px) {
          .gallery { column-count: 2; }
        }
        
        @media screen and (min-width: 600px) {
          .gallery { column-count: 3; }
        }
        
        @media screen and (min-width: 800px) {
          .gallery { column-count: 4; }
        }
        
        @media screen and (min-width: 1100px) {
          .gallery { column-count: 5; }
        }

        .item {
          box-sizing: border-box;
          break-inside: avoid;
          padding: 10px;
          text-align: center;
        }

        .item-content {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          font-size: 40px;
          box-sizing: border-box;
        }

        .zoom {
          display: block;
        }

        .zoom > img {
          height: auto;
          max-width: 100%;
        }

        .img-zoomed {
          position: fixed;
          top:  50%;
          left:  50%;
          transform: translate(-50%,-50%);
          display: block;
          max-width: calc(100vw - 40px);
          max-height: calc(100vh - 40px);
        }

      </style>

  </head>

  <body>

    <header class="page-header">
      <h1>Superawesome Gallery</h1>
      <p>Imagine you could make an image gallery simply by placing your images in a folder and uploading it to a webserver. This is it!</p>
    </header>

    <main class="gallery">

      <?php

        // Source: 
        // https://goo.gl/LX4i6F
        // 
        // In order to run a gallery locally from a folder on your computer run the 
        // following command from Terminal.app (make sure you are in the folder 
        // prior to running the command):
        // 
        // sudo php -S 127.0.0.1:80 -t .
        // 
        // This will make it possible for you to go to 127.0.0.1:80 in your browser 
        // and see your gallery. 

        function mtimecmp($a, $b) {
          $mt_a = filemtime($a);
          $mt_b = filemtime($b);

          if ($mt_a == $mt_b)
            return 0;
          else if ($mt_a < $mt_b)
            return -1;
          else
            return 1;
        }

        $images = glob('images/*.*');
        usort($images, "mtimecmp");

        for ($i = count($images) - 1; $i >= 0; $i--) {
            $image = $images[$i];
            echo '<figure class="item"><div class="item-content"><a href="#" class="zoom"><img src="' .$image. '" alt=""></a></div></figure>';
        }

      ?>

    </main>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>

    <script>
      $(document).ready(function() {

        $('.zoom').click(function(e) {
          $(this).children('img').clone().appendTo('body').addClass('img-zoomed');
          e.preventDefault();
        });

        $('body').on('click', '.img-zoomed', function(e) {
          $(this).remove();
          e.preventDefault();
        });

      });
    </script>

  </body>
</html>
